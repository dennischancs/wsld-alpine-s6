# !!only for download `wsld.tar` or `wsld.tar.gz`!!
# [wsld-alpine-s6](https://github.com/dennischancs/clearlinuxWSL/tree/main/wsld-alpine-s6)

an alpine wsl image supporting for [Rucadi/wsld](https://github.com/Rucadi/wsld), which is smaller and faster than ubuntu wsl image.


| images | [alpine](https://gitlab.com/dennischancs/wsld-alpine)  | [ubuntu](https://gitlab.com/ruben.cano96/wsld_image)|
| ------ | ------ | ------ |
|HDD | 325MB | 1128MB |
|RAM | 113MB | 144MB  |
